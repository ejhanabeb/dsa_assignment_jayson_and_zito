import  ballerina/http;

public type LearningMaterialArr LearningMaterial[];

# The Rest api communicates via different protocols like HTTP request protocols to perform functions like creating user profiles such as in the problem given aswell as updating and deleting
#
# + clientEp - Connector http endpoint
public client class Client {
    http:Client clientEp;
    public isolated function init(http:ClientConfiguration clientConfig =  {}, string serviceUrl = "http://localhost:9090/student_profile") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
    }
    # Create a student profile
    #
    # + return - Student Added successfully.
    remote isolated function  students(StudentProfile payload) returns error? {
        string  path = string `/students`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> post(path, request, targetType=http:Response);
    }
    # Update student profile
    #
    # + username - The username of the student
    # + return - OK
    remote isolated function  studentByusername(string username, StudentProfile payload) returns error? {
        string  path = string `/student/${username}`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> put(path, request, targetType=http:Response);
    }
    # Get student material for a particular student
    #
    # + username - The username of the student
    # + return - List of Student Material
    remote isolated function  material(string username) returns LearningMaterialArr|error {
        string  path = string `/students/${username}/material`;
        LearningMaterialArr response = check self.clientEp-> get(path, targetType = LearningMaterialArr);
        return response;
    }
}
