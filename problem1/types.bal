public type StudentProfile record {
    int username?;
    string 'last\-name?;
    string 'first\-name?;
    string[] 'preferred\-formats?;
    record  { string course?; string score?;} [] 'past\-subjects?;
};

public type LearningMaterial record {
    string course?;
    record  { record  { record  { string name?; string description?; string difficulty?;} [] audio?; record {}[] text?;}  required?; record  { record {}[] video?; record {}[] audio?;}  suggested?;}  'learning\-objects?;
};
