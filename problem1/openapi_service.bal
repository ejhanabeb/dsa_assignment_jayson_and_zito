import  ballerina/http;
import ballerina/io;

listener  http:Listener  ep0  = new (9090, config  = {host: "localhost"});

type student record {|
    string username;
    string lastname;
    string firstname;
    string[] preferred_formats;
    Past_subject past_subjects; 
    
|};
type  Past_subject record {|
    string course;
    string score;
|}  ;
type learningMaterial record {|
    string course?;
    record  { record  { record  { string name?; string description?; string difficulty?;} [] audio?; record {}[] text?;}  required?; record  { record {}[] video?; record {}[] audio?;}  suggested?;}  'learning\-objects?;
|};
student[] all_students = [];
learningMaterial[] all_LearningMaterial=[];

 service  /student_profile  on  ep0  {
        resource  function  post  students(@http:Payload student new_Student)  returns  json {
            io:println("Procesing new student");
            all_students.push(new_Student);
           return {"Add":"Successfully added "+ new_Student.username+"to the studentList"};
          }
    
        resource  function  put  student/[int  Update_userName](@http:Payload student  studentUpdate)  returns  json {
            string username_student = studentUpdate.username;
            string lastname_student = studentUpdate.lastname;
            string firstname_student = studentUpdate.firstname;
            string [] preferred_format_student = studentUpdate.preferred_formats;
            Past_subject past_subjects_student = studentUpdate.past_subjects;
             all_students [Update_userName].username = username_student; 
             all_students [Update_userName].lastname = lastname_student; 
             all_students [Update_userName].firstname = firstname_student; 
             all_students [Update_userName].preferred_formats = preferred_format_student; 
             all_students [Update_userName].firstname = firstname_student; 
             all_students [Update_userName].past_subjects = past_subjects_student;

    }
        resource  function  get  students/[int  username]()  returns  learningMaterial {
            return all_LearningMaterial[username];
    }
            
    }
