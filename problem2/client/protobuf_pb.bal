import ballerina/grpc;

public isolated client class functionManagerClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(Function|ContextFunction req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        Function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManager/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function add_new_fnContext(Function|ContextFunction req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        Function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManager/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function delete_fn(functionId|ContextFunctionId req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        functionId message;
        if (req is ContextFunctionId) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManager/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function delete_fnContext(functionId|ContextFunctionId req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        functionId message;
        if (req is ContextFunctionId) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManager/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function show_fn(functionId|ContextFunctionId req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        functionId message;
        if (req is ContextFunctionId) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManager/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function show_fnContext(functionId|ContextFunctionId req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        functionId message;
        if (req is ContextFunctionId) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functionManager/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function add_fns() returns (Add_fnsStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("functionManager/add_fns");
        return new Add_fnsStreamingClient(sClient);
    }

    isolated remote function show_all_fns(functionId|ContextFunctionId req) returns stream<functionVersion, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        functionId message;
        if (req is ContextFunctionId) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functionManager/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        FunctionVersionStream outputStream = new FunctionVersionStream(result);
        return new stream<functionVersion, grpc:Error?>(outputStream);
    }

    isolated remote function show_all_fnsContext(functionId|ContextFunctionId req) returns ContextFunctionVersionStream|grpc:Error {
        map<string|string[]> headers = {};
        functionId message;
        if (req is ContextFunctionId) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functionManager/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        FunctionVersionStream outputStream = new FunctionVersionStream(result);
        return {content: new stream<functionVersion, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function show_all_with_criteria() returns (Show_all_with_criteriaStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("functionManager/show_all_with_criteria");
        return new Show_all_with_criteriaStreamingClient(sClient);
    }
}

public client class Add_fnsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendMultipleFunctions(MultipleFunctions message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextMultipleFunctions(ContextMultipleFunctions message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class FunctionVersionStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|functionVersion value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|functionVersion value;|} nextRecord = {value: <functionVersion>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Show_all_with_criteriaStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendString(string message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextString(ContextString message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class FunctionManagerStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionManagerFunctionVersionCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFunctionVersion(functionVersion response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFunctionVersion(ContextFunctionVersion response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextStringStream record {|
    stream<string, error?> content;
    map<string|string[]> headers;
|};

public type ContextFunctionVersionStream record {|
    stream<functionVersion, error?> content;
    map<string|string[]> headers;
|};

public type ContextMultipleFunctionsStream record {|
    stream<MultipleFunctions, error?> content;
    map<string|string[]> headers;
|};

public type ContextFunction record {|
    Function content;
    map<string|string[]> headers;
|};

public type ContextFunctionId record {|
    functionId content;
    map<string|string[]> headers;
|};

public type ContextString record {|
    string content;
    map<string|string[]> headers;
|};

public type ContextFunctionVersion record {|
    functionVersion content;
    map<string|string[]> headers;
|};

public type ContextMultipleFunctions record {|
    MultipleFunctions content;
    map<string|string[]> headers;
|};

public type Function record {|
    string developersFullname = "";
    string email_address = "";
    functionId FunctionId = {};
    string implemented_language = "";
    string functionalityKeywords = "";
    int 'version = 0;
|};

public type functionId record {|
    int functionId = 0;
|};

public type functionVersion record {|
    int 'version = 0;
    Function functionVersionInfo = {};
|};

public type MultipleFunctions record {|
    Function[] multipleFunctions = [];
|};

const string ROOT_DESCRIPTOR = "0A0E70726F746F6275662E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22680A0F66756E6374696F6E56657273696F6E12180A0776657273696F6E180120012805520776657273696F6E123B0A1366756E6374696F6E56657273696F6E496E666F18022001280B32092E46756E6374696F6E521366756E6374696F6E56657273696F6E496E666F222C0A0A66756E6374696F6E4964121E0A0A66756E6374696F6E4964180120012805520A66756E6374696F6E4964228F020A0846756E6374696F6E122E0A12646576656C6F7065727346756C6C6E616D651801200128095212646576656C6F7065727346756C6C6E616D6512230A0D656D61696C5F61646472657373180220012809520C656D61696C41646472657373122B0A0A46756E6374696F6E496418032001280B320B2E66756E6374696F6E4964520A46756E6374696F6E496412310A14696D706C656D656E7465645F6C616E67756167651804200128095213696D706C656D656E7465644C616E677561676512340A1566756E6374696F6E616C6974794B6579776F726473180520012809521566756E6374696F6E616C6974794B6579776F72647312180A0776657273696F6E180620012805520776657273696F6E224C0A114D756C7469706C6546756E6374696F6E7312370A116D756C7469706C6546756E6374696F6E7318012003280B32092E46756E6374696F6E52116D756C7469706C6546756E6374696F6E733280030A0F66756E6374696F6E4D616E6167657212350A0A6164645F6E65775F666E12092E46756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565123D0A076164645F666E7312122E4D756C7469706C6546756E6374696F6E731A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112360A0964656C6574655F666E120B2E66756E6374696F6E49641A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512340A0773686F775F666E120B2E66756E6374696F6E49641A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565122F0A0C73686F775F616C6C5F666E73120B2E66756E6374696F6E49641A102E66756E6374696F6E56657273696F6E300112580A1673686F775F616C6C5F776974685F6372697465726961121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756528013001620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "protobuf.proto": "0A0E70726F746F6275662E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22680A0F66756E6374696F6E56657273696F6E12180A0776657273696F6E180120012805520776657273696F6E123B0A1366756E6374696F6E56657273696F6E496E666F18022001280B32092E46756E6374696F6E521366756E6374696F6E56657273696F6E496E666F222C0A0A66756E6374696F6E4964121E0A0A66756E6374696F6E4964180120012805520A66756E6374696F6E4964228F020A0846756E6374696F6E122E0A12646576656C6F7065727346756C6C6E616D651801200128095212646576656C6F7065727346756C6C6E616D6512230A0D656D61696C5F61646472657373180220012809520C656D61696C41646472657373122B0A0A46756E6374696F6E496418032001280B320B2E66756E6374696F6E4964520A46756E6374696F6E496412310A14696D706C656D656E7465645F6C616E67756167651804200128095213696D706C656D656E7465644C616E677561676512340A1566756E6374696F6E616C6974794B6579776F726473180520012809521566756E6374696F6E616C6974794B6579776F72647312180A0776657273696F6E180620012805520776657273696F6E224C0A114D756C7469706C6546756E6374696F6E7312370A116D756C7469706C6546756E6374696F6E7318012003280B32092E46756E6374696F6E52116D756C7469706C6546756E6374696F6E733280030A0F66756E6374696F6E4D616E6167657212350A0A6164645F6E65775F666E12092E46756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565123D0A076164645F666E7312122E4D756C7469706C6546756E6374696F6E731A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112360A0964656C6574655F666E120B2E66756E6374696F6E49641A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512340A0773686F775F666E120B2E66756E6374696F6E49641A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565122F0A0C73686F775F616C6C5F666E73120B2E66756E6374696F6E49641A102E66756E6374696F6E56657273696F6E300112580A1673686F775F616C6C5F776974685F6372697465726961121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756528013001620670726F746F33"};
}

