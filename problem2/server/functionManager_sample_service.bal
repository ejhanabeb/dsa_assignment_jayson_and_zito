import ballerina/grpc;

listener grpc:Listener ep = new (9090);
Function [] functionList =[];


@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "functionManager" on ep {

    remote function add_new_fn(Function value) returns string|error {
        functionList.push(value);
        return "Successfully added function "+ value.FunctionId.toString();
    }
    remote function delete_fn(functionId value) returns string|error {
    }
    remote function show_fn(functionId value) returns string|error {
        foreach Function item in functionList {
            if item.FunctionId==value{
        return "Function information"+ value.FunctionId.toString();
    }
        }
    }
    remote function add_fns(stream<MultipleFunctions, grpc:Error?> clientStream) returns string|error {
    }
    remote function show_all_fns(functionId value) returns stream<functionVersion, error?>|error {
    }
    remote function show_all_with_criteria(stream<string, grpc:Error?> clientStream) returns stream<string, error?>|error {
    }
}

